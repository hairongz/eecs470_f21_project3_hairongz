#!/usr/bin/bash
for file in test_progs/*.s; do
    #echo $file
    file=$(echo $file | cut -d'.' -f1)
    if ! [ -d $file ]; then 
       mkdir $file 
    fi 
    echo "Assembling $file"
    make assembly SOURCE=$file.s
    echo "Running $file"
    make 
    echo "Saving $file output"
    #grep "@@@" program.out > correct.program.out #get correct output from the unmodified groundtruth to the correct.program.out in order to compare with modified ones
    #cp -f correct.program.out /home/hairongz/Desktop/470/project3/eecs470_f21_project3_hairongz/$file #cp: copy the file into the new document file/ -f: force overrider 
    #cp -f writeback.out /home/hairongz/Desktop/470/project3/eecs470_f21_project3_hairongz/$file #cp: copy the file into the new document file/ -f: overrider
    #cp -f pipeline.out /home/hairongz/Desktop/470/project3/eecs470_f21_project3_hairongz/$file
    #both cp lines will be deleted in project 3
    
    #grep"CPI" program.out > $file.cpi.out 
    echo "##########"
    echo "Checking Assembly Write back results"
    #compare the writeback.out and program.out result. whether they are the same as the groundtruth
    diff writeback.out /home/hairongz/Desktop/470/project3/eecs470_f21_project3_hairongz/$file/writeback.out
    if [ $? -eq 0 ]; then 
        echo "Assembly Write back passed"
        grep "@@@" program.out > test_program.out 
        diff test_program.out /home/hairongz/Desktop/470/project3/eecs470_f21_project3_hairongz/$file/correct.program.out
        if [ $? -eq 0 ]; then
            echo "Assembly Correct"
        else
            echo "$file.s Failed!"
            break
        fi
    else 
        echo "$file.s Writeback Failed!"
        break
    fi 
done

#for file in test_progs/*.c; do 
#    file=$(echo $file | cut -d'.' -f1)
#    if ! [ -d $file ]; then 
#       mkdir $file 
#    fi 
#    echo "Compiling $file"
#    make program SOURCE=$file.c
#    echo "Running $file"
#    make 
#    echo "Saving $file output"
#    grep "@@@" program.out > correct.program.out 
#    cp -f correct.program.out /home/hairongz/Desktop/470/project3/pp3/$file 
#    cp -f writeback.out /home/hairongz/Desktop/470/project3/pp3/$file
#    #both cp line will be deleted in project 3
#    echo "##########"
#    echo "Checking Compiling Write back results"
#    diff writeback.out /home/hairongz/Desktop/470/project3/pp3/$file/writeback.out
#    if [ $? -eq 0 ]; then 
#        echo "Compiling Write back passed"
#        grep "@@@" program.out > test_program.out 
#        diff test_program.out /home/hairongz/Desktop/470/project3/pp3/$file/correct.program.out
#        if [ $? -eq 0 ]; then
#            echo "Compiling Correct"
#        else
#            echo "$file.c Failed!"
#            break
#        fi
#    else 
#        echo "$file.c Writeback Failed!"
#        break
#    fi 
#done